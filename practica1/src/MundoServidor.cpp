// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <vector>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void * hilo_comandos(void* d);

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close (fd); //clase fifo del logger
	//close (ident);
	//unlink("/tmp/mififo2");
	//munmap(pmem, sizeof(DatosMemCompartida));
	
	sconex.Close();//cerramos los sockets
	scomun.Close();
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	esfera.Dibuja();
	// e2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	// e2.Mueve(0.025f);
	esfera.Disminuir(0.00025f);
	/*pmem->raqueta1=jugador1;
	pmem->raqueta2=jugador2;
	pmem->esfera=esfera;
	pmem->iddle=iddle;*/

// para el periodo de inactividad


	if(pmem->accion1== 1)
		OnKeyboardDown('w',0,0);
	else if(pmem->accion1==-1)
		OnKeyboardDown('s',0,0);
	if(pmem->accion2== 1)
		OnKeyboardDown('o',0,0);
	else if(pmem->accion2==-1)
		OnKeyboardDown('l',0,0);
	
//pasar informacion de servidor a cliente

	char cad_s[200];
	sprintf(cad_s,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);
	//write (ident,cad,strlen(cad)+1);

	sock= scomun.Send(cad_s, sizeof(cad_s));
	if (sock <0) perror ("Error enviando coordenadas");
		
// codigo de las interacciones				

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		//string punt2="Jugador 2 marca 1 punto, lleva un total de"/*+puntos2+"puntos"*/;	
		//char* p2[] =new char [punt2.length() +1];
		//strcpy (p2, punt2.c_str());
		char data [1024];
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(data, "Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
		write (fd,data,strlen(data)+1);//esto es para comunicar con el logger que se ha anotado un punto
	}

	if(fondo_dcho.Rebota(esfera))
	{	
		//string punt1="Jugador 1 marca 1 punto, lleva un total de"/*+puntos1+"puntos"*/;
		//char* p1[] =new char [punt1.length() +1];
		//strcpy (p1, punt1.c_str());
		char data [1024];
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(data, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
		write (fd,data,strlen(data)+1);//esto es para comunicar con el logger que se ha anotado un punto
	}

	
	
	int j;
	for(j=0;j<paredes.size();j++)
	{
		//paredes[j].Rebota(e2);
		paredes[j].Rebota(jugador1);
		paredes[j].Rebota(jugador2);
	}

	/*jugador1.Rebota(e2);
	jugador2.Rebota(e2);
	if(fondo_izq.Rebota(e2))
	{
		
		char data [1024];
		e2.centro.x=0;
		e2.centro.y=rand()/(float)RAND_MAX;
		e2.velocidad.x=2+2*rand()/(float)RAND_MAX;
		e2.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(data, "Jugador 2 marca 1 punto, lleva un total de %d puntos", puntos2);
		write (fd,data,strlen(data)+1);
	}

	if(fondo_dcho.Rebota(e2))
	{
		char data [1024];		
		e2.centro.x=0;
		e2.centro.y=rand()/(float)RAND_MAX;
		e2.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		e2.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(data, "Jugador 1 marca 1 punto, lleva un total de %d puntos", puntos1);
		write (fd,data,strlen(data)+1);
	}*/
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	/*switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}*/
}

void CMundo::Init()
{	
	Plano p;

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

// Pipe del Logger

	fd=open("/tmp/mififo1",O_WRONLY);
	
// Creacion del bot	
	
	b = open("/tmp/bot", O_CREAT|O_RDWR, 0666);
	write(b, &mem,sizeof(mem));
	pmem = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, b, 0);
	close(b);

	//Sockets
	//sprintf(ip,"127.0.0.1");
	sock=sconex.InitServer((char *) "127.0.0.1",1800);
		if (sock < 0) perror("Fallo al enlazar la conexión\n");
		else {
			printf("Conexion establecida\n");
			scomun=sconex.Accept();}
	
	sock=scomun.Receive(nombre_s,sizeof(nombre_s)); // Recibe el nombre del cliente conectado a traves del socket
		if (sock < 0) perror("Fallo al recibir el nombre del cliente\n");
		else printf("El nombre del cliente es: %s", nombre_s); // imprime por pantalla el nombre recibido

	pthread_create(&thid1, NULL, hilo_comandos, this); // Se crea el thread
//pipe entre servidor y cliente, el servidor lo abre, no lo crea

	//ident=open("/tmp/mififo2",O_WRONLY);

//Abre el fifo que conecta cliente con el hilo

	//identcs=open("/tmp/mififo3", O_RDONLY);
	//Thread
	
	
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[100];
            //read(identcs, cad, sizeof(cad));
	
	sock = scomun.Receive(cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}


void* hilo_comandos(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
}


