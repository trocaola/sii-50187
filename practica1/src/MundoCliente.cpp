// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <vector>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<fcntl.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//close (ident);
	//munmap(pmem, sizeof(DatosMemCompartida));
	//close (identcs);
	//unlink("/tmp/mififo3");

	scomser.Close();
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();

	esfera.Dibuja();
	// e2.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.Disminuir(0.00025f);
	pmem->raqueta1=jugador1;
	pmem->raqueta2=jugador2;
	pmem->esfera=esfera;
	pmem->iddle=iddle;

// para el periodo de inactividad

	if(pmem->accion1== 1)
		OnKeyboardDown('w',0,0);
	else if(pmem->accion1==-1)
		OnKeyboardDown('s',0,0);
	if(pmem->accion2== 1)
		OnKeyboardDown('o',0,0);
	else if(pmem->accion2==-1)
		OnKeyboardDown('l',0,0);
//recoger la informacion del servidor en el cliente

	char cad_c[200];
	//read(ident, cad, sizeof(cad));
	
	sock= scomser.Receive(cad_c,sizeof(cad_c));
		if (sock <0) perror ("Fallo al recibir las coordenadas del servidor\n");
		else sscanf(cad_c,"%f %f %f %f %f %f %f %f %f %f %d %d\n", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
// codigo de las interacciones				

	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{

		char data [1024];
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
	}

	if(fondo_dcho.Rebota(esfera))
	{	
		
		char data [1024];
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
	}

	
	int j;
	for(j=0;j<paredes.size();j++)
	{
		paredes[j].Rebota(jugador1);
		paredes[j].Rebota(jugador2);
	}
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	
	char cad_teclas[100];
	sprintf(cad_teclas,"%c",key);
	//write (identcs,cad,strlen(cad)+1);

	sock=scomser.Send(cad_teclas, sizeof(cad_teclas)); //El cliente envia las teclas
		if (sock < 0) perror("Fallo al enviar las teclas al servidor\n");

	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	}

	
}

void CMundo::Init()
{	
	Plano p;

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

// Pipe del Logger

	//fd=open("/tmp/mififo1",O_WRONLY);
	
// Creacion del bot	
	
	b = open("/tmp/bot", O_CREAT|O_RDWR, 0666);
		if (b<0) perror ("ERROR AL ABRIR EL BOT");
	write(b, &mem,sizeof(mem));
	pmem = (DatosMemCompartida*) mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, b, 0);
	
	//Socket
	printf("Introduce un nombre\n");
	scanf ("%s", nombre);
	sock= scomser.Connect((char *) "127.0.0.1",1800); //Importante el cast!!!!!!!!!!!!!!
	if (sock < 0) perror ("Fallo al conectarse con la IP del servidor\n");
	else{
			sock=scomser.Send(nombre, sizeof(nombre)); //El cliente envia el nombre al servidor 
			if (sock < 0) perror ("Fallo al enviar el nombre al servidor\n");
		}

//se crea el pipe en modo read del servidor-cliente

	//int status, statussc;
	//status = mkfifo("/tmp/mififo2", 0777);
	//ident=open("/tmp/mififo2",O_RDONLY);

//pipe para cliente-> servidor

	//int statuscs=mkfifo("/tmp/mififo3", 0777);
	//identcs=open("/tmp/mififo3",O_WRONLY);
	
}



