// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "Esfera.h"
//#include "Raqueta.h"
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include "DatosMemCompartida.h"
#include <pthread.h>
#include <Socket.h>
using namespace std;

class CMundo  
{
public:

	//Sockets
	Socket scomser; //Socket para la comunicacion con el servidor
	char nombre[15];
	int sock;
	char ip[1024];
	void Init();
	CMundo();

	int b;

	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	// Esfera e2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int iddle;

	int puntos1;
	int puntos2;

	//int fd;
	//int ident, identcs; // tuberia serv-cliente y cliente-serv(pthread)

	DatosMemCompartida mem;
	DatosMemCompartida *pmem; 
	
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
